# How to create an Azure Subscription using Endava's credentials

## Login to Microsoft Azure Portal
1. Use the web browser of your preference and access to this url http://portal.azure.com.
2. Enter your endava email and password (the same credentials to logon in the Endava domain).

## Create an Azure Subscription.
1. After a successful login, in your Azure home page, go to the upper side of the web page and search for "Subscriptions".

![alt text][Image01]

[Image01]:images/Image01.png "Image01"

2. In the Subscriptions blade, click on "Add" button seen in the figure. A new web page will be opened. If credentials are asked, ensure to use the same Endava's credentials re-entering them again.

![alt text][Image02]

[Image02]:images/Image02.png "Image02"

3. In the "Add subscription" page, select the "Free Trial" offer option clicking on it. As the step above, credentials would be asked again.

![alt text][Image07]

[Image07]:images/Image07.png "Image07"

4. There will be opened a new web page called "Azure free account sign up". Ensure to fill up all the required fields like: Country, Names. Once finished, click on "Next" button.

![alt text][Image03]

[Image03]:images/Image03.png "Image03"

5. In the "Agreement" option, ensure to agree with the terms. Finally, click on "Sign Up". Most probably, credentials would be required.

![alt text][Image04]

[Image04]:images/Image04.png "Image04"


## Confirm if the Subscription was created
1. To confirm that the subscription was created successfully, check in the Azure home page in the upper side in the notification icon as shown in the figure. Once clicked on it, a notification should be opened and there will be one confirming the 200$ of credits for the free trial subscription.

![alt text][Image05]

[Image05]:images/Image05.png "Image05"

2. Also, in your Azure home page, go to the upper side of the web page and search for "Subscriptions" as done previously.
3. In the "Subscription" blade, the new "Free Trial" subscription should be showned as the figure. Also, confirm that its status is "Active".

![alt text][Image06]

[Image06]:images/Image06.png "Image06"

4. Enjoy Azure Cloud.

> If there are issues when creating your Azure Subscription or more information is needed, please, contact Carlos Gonzalez (carlos.gonzalez@endava.com) or Vanessa Celis (daniela.celis@endava.com).