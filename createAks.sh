#!/bin/bash
read -p "Enter your Endava email: " username
read -s -p "Enter your password: " password
if [[ "$username" != *"@endava.com"*  ]]; then username="$username@endava.com"; fi
if [[ $(az login -u $username -p $password) ]]; then
    echo "Logon successfully, executing commands..."
    username=${username%"@endava.com"}
    resourceGroup="aksWorkShop"
    location="eastus2"
    servicePrincipal="aks-$username"
    vNetTemplate="vNet.json"
    aksTemplate="aks.json"
    aksName="akstest"
    az group create --name $resourceGroup --location $location
    appid=$(az ad sp create-for-rbac -n $servicePrincipal --role contributor --query appId)
    password=$(az ad sp create-for-rbac -n $servicePrincipal --role contributor --query password)
    appid=${appid#\"} && appid=${appid%\"}
    password=${password#\"} && password=${password%\"}
    az group deployment create --template-file $vNetTemplate --resource-group $resourceGroup --verbose
    az group deployment create --template-file $aksTemplate --resource-group $resourceGroup --parameters \
                                        spClientId=$appid \
                                        spSecret=$password \
                                        --verbose
    az aks install-cli
    az aks Get-Credentials --resource-group $resourceGroup --name $aksName --overwrite-existing
    kubectl config set-context $(kubectl config current-context)
    echo "AKS cluster created successfully!"
else
    echo "Wrong credentials, review them!"
fi